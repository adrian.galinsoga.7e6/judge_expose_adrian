import org.jetbrains.exposed.sql.Table

object Problema : Table() {
    val id = integer("id").autoIncrement()
    val tema = varchar("tema", 100)
    val enunciado = text("enunciado")
    val input = text("input")
    val output = text ("output")
    val solucion = bool("solucion")
    val intentos = integer("intentos")

}