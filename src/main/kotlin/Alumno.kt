import Alumno.id
import Alumno.problemaResuelto
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.transaction

object Alumno : Table() {
    val id = integer("id").autoIncrement()
    val nombre = varchar("nombre", 50)
    val apellido = varchar("apellido", 50)
    val email = varchar("email", 70)
    val contrasena = varchar("contrasena", 70)
    val problemaResuelto = varchar("problemasSolucionados", 500).nullable()


    fun loginAlumno(email: String, contrasena: String): Boolean {
        val alumnos = Alumno.selectAll().toList()

        if (alumnos.isEmpty()) {
            println("No hay profesores")
            return false
        }
        val emailAlumno = alumnos.find { it[Alumno.email] == email }

        if (emailAlumno != null) {
            val guardarContrasena = emailAlumno[Alumno.contrasena]

            if (guardarContrasena == contrasena) {
                println("¡Inicio sesión válido!")
                println()
                println("Cargando . . .")
                Thread.sleep(3000)
                return true
            }


        }
        println("¡Error inicio sesión!")
        return false
    }

    fun seleccionarAlumnos(): List<ResultRow> {
        return transaction {
            Alumno.selectAll().toList()
        }
    }

    fun alumnoId(email: String): Int? {
        var alumnoId: Int? = null

        transaction {
            val result = Alumno.select { Alumno.email eq email }.singleOrNull()
            alumnoId = result?.get(Alumno.id)
        }

        return alumnoId
    }

    private fun problemaSolucionado(problemaId: Int, alumnoEmail: String): Boolean {
        val problema = Problema.select { Problema.id eq problemaId }.singleOrNull()


        if (problema != null) {
            println("Numero de Pronlema $problemaId")
            println("Tema: ${problema[Problema.tema]}")
            println("Enunciado: ${problema[Problema.enunciado]}")
            println("Entrada: ${problema[Problema.input]}")
            println("Salida: ")

            var solucionado = false
            var intentos = problema[Problema.intentos]

            while (!solucionado) {
                println()
                println("¿Solucionar este problema?")
                println("1. Sí")
                println("2. No, quiero volver al menú principal")

                when (readln()) {
                    "1" -> {
                        println("Introduce tu solución:")
                        val userSolution = readln()

                        if (userSolution == problema[Problema.output]) {
                            solucionado = true
                            println("Has resuelto el problema.")

                            transaction {
                                Problema.update({ Problema.id eq problemaId }) {
                                    it[Problema.intentos] = intentos
                                }

                                val studentId = alumnoId(alumnoEmail)
                                if (studentId != null) {
                                    actualizarProblemas(studentId, problemaId)
                                } else {
                                    println("No se encontró un estudiante con el correo electrónico: $alumnoEmail")
                                }
                            }
                        } else {
                            intentos++
                            println("Tu solución es incorrecta. Sigue intentándolo.")
                        }
                    }

                    "2" -> {
                        println("Volviendo al menú principal...")
                        return false
                    }

                    else -> {
                        println("Opción inválida. Por favor, selecciona una opción válida.")
                    }
                }
            }

            return true
        } else {
            println("No se encontró un problema con ID: $problemaId")
            return false
        }
    }

    private fun actualizarProblemas(alumnoId: Int, problemaId: Int) {
        transaction {
            val alumno = Alumno.select { Alumno.id eq alumnoId }.singleOrNull()

            if (alumno != null) {
                val problemsSolvedString = alumno[problemaResuelto]
                val solvedProblems = problemsSolvedString?.split(",")?.map { it.toInt() }?.toMutableList()
                    ?: mutableListOf()

                if (!solvedProblems.contains(problemaId)) {
                    solvedProblems.add(problemaId)
                    val updatedProblemsSolved = solvedProblems.joinToString(",")
                    Alumno.update({ Alumno.id eq alumnoId }) {
                        it[problemaResuelto] = updatedProblemsSolved
                    }
                }
            } else {
                println("No se encontró un estudiante con ID: $alumnoId")
            }
        }
    }

    private fun problemasSolucionadosAlumno(alumnoId: Int): List<Int> {
        val alumno = Alumno.select { id eq alumnoId }.singleOrNull()

        return alumno?.get(problemaResuelto)
            ?.split(",")
            ?.mapNotNull { it.toIntOrNull() }
            ?: emptyList()
    }

    private fun detalleProblema(problema: ResultRow) {
        val id = problema[Problema.id]
        val tema = problema[Problema.tema]
        val enunciado = problema[Problema.enunciado]
        val input = problema[Problema.input]
        val output = "?"
        val intentos = problema[Problema.intentos]

        println("--------------------------------")
        println()
        println("ID: $id")
        println("Tema: $tema")
        println("Enunciado: $enunciado")
        println("Input: $input")
        println("Output: $output")
        println("Intents: $intentos")
    }

    fun seguimientoDeProblemas(email: String) {
        val alumnoId = alumnoId(email)
        val problemasSolucionados = problemasSolucionadosAlumno(alumnoId!!).toMutableList()

        while (true) {
            val problemasRestantes = transaction {
                Problema.select { Problema.id notInList problemasSolucionados }
                    .orderBy(Problema.id, SortOrder.ASC)
                    .limit(1)
                    .singleOrNull()
            }

            if (problemasRestantes != null) {
                val idProblema = problemasRestantes[Problema.id]
                val temaProblema = problemasRestantes[Problema.tema]
                val enunciadoProblema = problemasRestantes[Problema.enunciado]
                val inpuProblema = problemasRestantes[Problema.input]
                val outputProblema = problemasRestantes[Problema.output]
                val intentosProblema = problemasRestantes[Problema.intentos]

                println("Problema número: $idProblema")
                println("Tema: $temaProblema")
                println("Enunciado: $enunciadoProblema")
                println("Entrada: $inpuProblema")
                println("Salida: $outputProblema")
                println("Intentos: $intentosProblema")

                println()
                println("¿Quieres solucionar este problema?")
                println("1. Sí")
                println("2. No, volver al menú principal")

                when (readln()) {
                    "1" -> {
                        val solucionado = problemaSolucionado(idProblema, email)
                        if (solucionado) {
                            problemasSolucionados.add(idProblema)
                        } else {
                            println("Volviendo al menú principal.")
                            break
                        }
                    }

                    "2" -> {
                        println("Volviendo al menú principal.")
                        break
                    }

                    else -> {
                        println("Opción inválida. Por favor, selecciona una opción válida.")
                    }
                }
            } else {
                println("¡Felicidades! Has completado todos los problemas.")
                println("Vuelve a seleccionar esta opción para continuar aprendiendo cuando quieras.")
                break
            }
        }


        transaction {
            val actualizarProblemas = problemasSolucionados.joinToString(",")
            val alumno = Alumno.select { Alumno.id eq alumnoId }.singleOrNull()
            if (alumno != null) {
                Alumno.update({ Alumno.id eq alumnoId }) {
                    it[problemaResuelto] = actualizarProblemas
                }
            } else {
                println("No se encontró un estudiante con ID: $alumnoId")
            }
        }
    }

    fun verProblemas(email: String) {
        val problemas = transaction {
            Problema.selectAll().toList()
        }

        if (problemas.isEmpty()) {
            println("No hay problemas registrados.")
        } else {
            println("Lista de problemas:")
            println()
            val studentId = alumnoId(email)
            val solvedProblemIds = idProblemasSolucionados(studentId)

            val availableProblems = problemas.filterNot { it[Problema.id] in solvedProblemIds }

            if (availableProblems.isEmpty()) {
                println("Ya has resuelto todos los problemas disponibles.")
                println()
                logOut()
            } else {
                for (problem in availableProblems) {
                    repeat(2) { println("") }
                    detalleProblema(problem)
                }

                println()
                println("¿Te gustaría solucionar algún problema?")
                println("1. Si  | 2. No")

                when (readln()) {
                    "1" -> {
                        val alumno = Alumno
                        println("¿Qué problema quieres solucionar?")
                        val problem = readln()

                        val selectedProblemId = problem.toInt()
                        if (selectedProblemId in solvedProblemIds) {
                            println("Este problema ya ha sido solucionado.")
                        } else {
                            alumno.problemaSolucionado(selectedProblemId, email)
                        }
                    }

                    "2" -> logOut()
                }
            }
        }
    }

    fun idProblemasSolucionados(alumnoId: Int?): List<Int> {
        return if (alumnoId != null) {
            transaction {
                Problema.slice(Problema.id)
                    .select {
                        Problema.id inList
                                (Alumno.slice(problemaResuelto)
                                    .select { Alumno.id eq alumnoId }
                                    .singleOrNull()
                                    ?.let { it[problemaResuelto]?.split(",")?.map(String::toInt) }
                                    ?: emptyList()
                                        )
                    }
                    .map { it[Problema.id] }
                    .sorted()
            }
        } else {
            emptyList()
        }
    }

    fun verProblemasSolucionados(alumnoEmail: String) {
        val studentId = alumnoId(alumnoEmail)

        if (studentId != null) {
            val solvedProblemIds = transaction {
                Problema.slice(Problema.id)
                    .select {
                        Problema.id inList
                                (Alumno.slice(problemaResuelto)
                                    .select { Alumno.id eq studentId }
                                    .singleOrNull()
                                    ?.let { it[problemaResuelto]?.split(",")?.map(String::toInt) }
                                    ?: emptyList()
                                        )
                    }
                    .map { it[Problema.id] }
                    .sorted()
            }

            if (solvedProblemIds.isNotEmpty()) {
                val solvedProblems = transaction {
                    Problema.select { Problema.id inList solvedProblemIds }
                        .orderBy(Problema.id, SortOrder.ASC)
                        .toList()
                }


                println("Problemas resueltos:")
                for (row in solvedProblems) {
                    val problemId = row[Problema.id]
                    val tema = row[Problema.tema]
                    val enunciado = row[Problema.enunciado]

                    println("ID: $problemId")
                    println("Tema: $tema")
                    println("Enunciado: $enunciado")
                    println()
                }
            } else {
                println("No has resuelto ningún problema aún.")
            }
        } else {
            println("No se encontró un estudiante con el correo electrónico: $alumnoEmail")
        }
    }
}



