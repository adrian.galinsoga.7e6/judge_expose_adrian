# JutgeITB Exposed - Readme

## - ANTES DE INICIAR EL PROGRAMA POR PRIMERA VEZ

CREATE DATABASE jutgeitb_exposed;
\c jutgeitb_exposed;

## - DESPUES DE EJECUTAR EL PROGRAMA POR PRIMERA VEZ

## Configuración de los datos
INSERT INTO profesor (nombre, apellidos, correo, contrasena)
VALUES ('Jordi', 'Cidoncha Gomez', 'jordi@itb.cat', 'jordi123');

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Ejercicios de tipos de datos, transformaciones y operaciones', 'Lee un valor con decimales e imprime el doble.', '123.456', '246.912', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Ejercicios de tipos de datos, transformaciones y operaciones', 'Lee el precio original y el precio actual e imprime el descuento (en %).', '540 420', '22.22', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Ejercicios de tipos de datos, transformaciones y operaciones', 'Lee el diámetro de una pizza redonda e imprime su superficie. puedes usar Math.pI para escribir el valor de pi.', '55', '2375.83', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Ejercicios de tipos de datos, transformaciones y operaciones', 'Haz un programa que reciba una temperatura en grados Celsius y la convierta en grados Fahrenheit', '-3.2', '26.2', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Ejercicios de tipos de datos, transformaciones y operaciones', 'Haz un programa que sume 1 segundo a un número determinado de segundos.', '59', '0', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Estructuras de control condicionales', 'Escribe un programa que, dados 3 números enteros, imprima el mayor por pantalla.', '155 65 155', '155', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Estructuras de control condicionales', 'por la input recibirás el número de horas que ha trabajado en una semana, en formato entero.', '55', '2500', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Estructuras de control condicionales', 'El usuario escribe un entero y se imprime true si existe un billete de euros con la cantidad introducida, false en cualquier otro caso.', '50', 'true', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Estructuras de control condicionales', 'Queremos hacer un programa que nos indique si un valor es par o impar.', '24', 'par', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Estructuras de control condicionales', 'Haz un programa que sume 1 segundo a una hora dada.', '8 59 59', '09:00:00', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Estructuras de control iterativas', 'Escribe un programa que dado un número entero imprima la suma de todos los enteros hasta llegar a este.', '5', '15', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Estructuras de control iterativas', 'Escribe un programa que dado dos números enteros imprima el resultado de elevar el primero por el segundo.', '2 32', '4294967296', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Estructuras de control iterativas', 'Escribe un programa que dado un número entero, escriba el número al revés.', '123456', '654321', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Estructuras de control iterativas', 'Escribe un programa que dado un número entero, indique si este es primo o no.', '128', 'No es primo', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Estructuras de control iterativas', 'Escribe un programa que reciba un número entero n y calcule su factorial.', '12', '479001600', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Ejercicios con arrays, strings y listas', 'Haz un programa que reciba por parámetro una secuencia no vacía de enteros y calcule la suma de todos los valores.', '-3 10 0 -2 8', '13', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Ejercicios con arrays, strings y listas', 'por la input recibirás un número entero, que representa un número de dni, es decir, un número de 8 cifras exactas.', '65004203', '65004203Q', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Ejercicios con arrays, strings y listas', 'Haz un programa que reciba por parámetro una secuencia no vacía de enteros y muestre el mínimo y el máximo valor dentro de la secuencia.', '1 5 6 7 10 19 23 43 198 192 200 0', '0 200', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Ejercicios con arrays, strings y listas', 'Haz un programa que reciba por parámetro una secuencia no vacía de enteros, y que escriba cuántos son iguales al último.', '-3 10 0 -2 8', '0', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Ejercicios con arrays, strings y listas', 'Haz un programa que reciba por parámetro una secuencia no vacía de enteros y muestre los valores de esta secuencia que son repetidos.', '5 4 123 345 65 324 1 2 4 66', '4', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Ejercicios con arrays, strings y listas', 'Haz un programa que reciba por parámetro una secuencia no vacía de enteros y muestre el valor que falta en la secuencia.', '1 2 3 4 5 6 7 9 10 11', '8', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Ejercicios con arrays, strings y listas', 'Haz un programa que, dada una secuencia de naturales recibida por parámetro indique si hay alguno que sea igual a la suma de todos los otros.', '1 0 3 3 1 0 2', 'no', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Ejercicios con arrays, strings y listas', 'Haz una aplicación que dada una secuencia con sólo ( y ), diga si los paréntesis cierran correctamente.', '(()()))(', 'no', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Ejercicios con arrays, strings y listas', 'Haz un programa que lea un String y invierta el orden de las palabras dentro del mismo.', 'Aquest String no està invertit', 'invertit està no String Aquest', 0);

INSERT INTO problema (tema, enunciado, input, output, intentos)
VALUES ('Ejercicios con arrays, strings y listas', 'Haz un programa que lea un String e invierta el orden de las palabras dentro de este y, además, invierta el orden de las letras de cada palabra.', 'Aquest potser si', 'is restop tseuqA', 0);

