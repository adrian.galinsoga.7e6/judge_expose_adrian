import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import kotlin.system.exitProcess

fun main (){
    val url = "jdbc:postgresql://localhost:5432/jutgitb" // URL de conexión a la base de datos
    val driver = "org.postgresql.Driver" // Controlador JDBC para PostgreSQL
    val user = "postgres"

    try {
     Database.connect(url,driver, user)

     val tablas = listOf(Alumno, Profesor, Problema)

     transaction {
         SchemaUtils.create(*tablas.toTypedArray())
     }

    }catch (e: Exception) {
        println("Error al conectar a la base de datos: ${e.message}")
        e.printStackTrace() // Obtenemos más detalles sobre la excepción
    }

    menuBienvenida()
}

fun menuBienvenida() {
    println("Bienvenid@ a JutgeITB")
    println("1. Profesor | 2. Alumno | 3. Crear cuenta | 4. Salir")

    when (readln()) {
        "1" -> {
            val profesor = Profesor
            println("Introduzca su correo electrónico: ")
            val email = readln()
            println("Introduzca su contraseña: ")
            val contrasena = readln()
            transaction {
                val loginExitoso = profesor.loginProfesor(email, contrasena)

                if (loginExitoso) {
                    menuProfesor(email)
                } else {
                    println("Error al iniciar sesión")
                }
            }
        }
        "2" -> {
            val alumno = Alumno
            println("Introduzca su correo electrónico: ")
            val email = readln()
            println("Introduzca su contraseña: ")
            val contrasena = readln()

            transaction {
                val loginSuccesfull = alumno.loginAlumno(email, contrasena)

                if (loginSuccesfull) {
                    menuAlumno(email)
                } else {
                    println("Error al iniciar sesión")
                }
            }
        }
        "3" -> crearCuenta()
        "4" -> logOut()
    }
}

fun logOut() {
    println("¡Gracias por utilizarme, nos vemos!")
    Thread.sleep(1000)
    exitProcess(0)
}

fun menuProfesor(email:String){
    val teacherData = transaction {
            Profesor.select {Profesor.email eq email }.singleOrNull()
    }
    val nombreProfesor = teacherData?.get(Profesor.nombre)
    val apellidoProfesor = teacherData?.get(Profesor.apellido)

    repeat(10) { println("")}
    println("             Bienvenid@ a JutgeITB |$nombreProfesor $apellidoProfesor          ")
    println()
    println("1. Crear nuevos problemas        |  2. Corregir alumno                             ")

    when (readln()) {
       "1" -> {
           val profesor = Profesor
           profesor.nuevoProblema()
        }
       "2" -> corregirAlumno()
    }

}

fun corregirAlumno(){
    val alumnos = Alumno.seleccionarAlumnos().sortedBy { it[Alumno.id] }

    if (alumnos.isNotEmpty()) {
        println("Lista de estudiantes:")
        alumnos.forEach { alumno ->
            println()
            println("${alumno[Alumno.id]}")
            println("Nombre: ${alumno[Alumno.nombre]}")
            println("Apellidos: ${alumno[Alumno.apellido]}")
            println()
        }
        println("¿A qué alumno quieres corregir? (Introduce el ID del alumno):")
        val studentId = readln().toInt()
        val profesor = Profesor
        profesor.correccionAlumno(studentId)
    } else {
        println("No hay estudiantes registrados.")
    }
}

fun menuAlumno(email: String) {
    val actualAlumno = transaction { Alumno.select { Alumno.email eq email }.singleOrNull() }
    val alumno = Alumno
    val emailAlumno = actualAlumno?.get(Alumno.email)
    val nombreAlumno = actualAlumno?.get(Alumno.nombre)
    val apellido = actualAlumno?.get(Alumno.apellido)


    repeat(10) { println("") }
    println("             Bienvenid@ a JutgeITB | $nombreAlumno $apellido          ")
    println()
    println("1. Seguir itinerario de aprendizaje  |  2. Ver todos los problemas                 ")
    println("3. Ver problemas resueltos           |  4. Cerrar sesión                           ")

    when (readln()) {
        "1" -> Alumno.seguimientoDeProblemas(email)
        "2" -> Alumno.verProblemas(email)
        "3" -> emailAlumno?.let { Alumno.verProblemasSolucionados(it) }
        "4" -> logOut()

    }
}

fun crearCuenta(){
    println("* Ingrese su nombre: ")
    val nombre = readln()
    println("* Ingrese sus apellidos: ")
    val apellido = readln()
    println("* Ingrese su correo electrónico: ")
    val email = readln()
    println("* Ingrese su contraseña: ")
    val contrasena = readln()


    if (nombre.isBlank()) {
        println("Por favor, introduzca un nombre de usuario")
    } else if (apellido.isBlank()) {
        println("Por favor, introduzca los apellidos.")
    } else if (email.isBlank()) {
        println("Por favor, introduzca un correo electrónico")
    } else if (!email.contains("@")) {
        println("El correo electrónico ingresado no es válido")
    }

    transaction {
        Alumno.insert {
            it[Alumno.nombre] = nombre
            it[Alumno.apellido] = apellido
            it[Alumno.email] = email
            it[Alumno.contrasena] = contrasena
        }
    }

    println("¡Cuenta creada exitosamente!")

    menuBienvenida()
}
