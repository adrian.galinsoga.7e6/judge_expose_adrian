import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

object Profesor : Table() {
    val nombre = varchar("nombre", 50)
    val apellido = varchar("apellido", 50)
    val email = varchar("email", 70)
    val contrasena = varchar("contrasena", 70)

    fun loginProfesor(email: String, contrasena: String): Boolean {
        val profesores = Profesor.selectAll().toList()

        if (profesores.isEmpty()) {
            println("No hay profesores")
            return false
        }
        val emailProfesor = profesores.find { it[Profesor.email] == email }

        if (emailProfesor != null) {
            val contrasenaProfesor = emailProfesor[Profesor.contrasena]

            if (contrasenaProfesor == contrasena) {
                println("¡Inicio de sesión exitoso!")
                println()
                Thread.sleep(3000)
                println("Cargando . . .")
                return true
            }

        }
        println("Inicio de sesión fallido.")
        return false
    }

    fun nuevoProblema() {
        println("Introduzca el tema: ")
        val tema = readln()
        println("Introduzca el enunciado: ")
        val enunciado = readln()
        println("Ejemplo de entrada quieres introducir?: ")
        val input = readln()
        println("Ejemplo de salida quieres introducir?: ")
        val output = readln()

        transaction {
            Problema.insert {
                it[Problema.tema] = tema
                it[Problema.enunciado] = enunciado
                it[Problema.input] = input
                it[Problema.output] = output
                it[intentos] = 0
            }
        }
        println("Problema creado!")
    }


    fun correccionAlumno(alumnoId: Int) {
        transaction {
            val alumno = Alumno.select { Alumno.id eq alumnoId }.singleOrNull()

            if (alumno != null) {
                println()
                println("Alumno seleccionado: ${alumno[Alumno.id]}")
                println("Nombre: ${alumno[Alumno.nombre]}")
                println("Apellidos: ${alumno[Alumno.apellido]}")
                println()

                val problemsSolvedString = alumno[Alumno.problemaResuelto]
                val solvedProblemsIds = problemsSolvedString?.split(",")?.map { it.toInt() } ?: emptyList()

                val contadorProblemas = Problema.selectAll().count()
                val contadorSolucionados = solvedProblemsIds.size

                if (contadorSolucionados > 0) {
                    println("Problemas resueltos:")
                    val problemasSolucionados = Problema.select { Problema.id inList solvedProblemsIds }
                        .orderBy(Problema.id, SortOrder.ASC)
                    problemasSolucionados.forEach { problem ->
                        println("Id: ${problem[Problema.id]}")
                        println("Tema: ${problem[Problema.tema]}")
                        println("Enunciado: ${problem[Problema.enunciado]}")
                        println()
                    }

                    val media = (contadorSolucionados.toDouble() / contadorProblemas) * 10.0
                    println("Nota media: %.2f".format(media))
                } else {
                    println("El estudiante no ha resuelto problemas.")
                }
            } else {
                println("No se encontró el ID proporcionado del estudiante.")
            }
        }
    }
}
